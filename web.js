var mysql     = require('mysql'),
	dbConfig  = {
		host: 'us-cdbr-east-05.cleardb.net',
		user: 'bc78531c4a1c2f',
		password: 'b18382bd',
		database: 'heroku_b359eb4d7034732'
	},
	connection,
	express   = require("express"),
	app       = express(),
	port      = Number(process.env.PORT || 5000),
	io        = require('socket.io').listen(app.listen(port));


app.use(express.static(__dirname + '/public'));


function handleDisconnect() {
	connection = mysql.createConnection(dbConfig);

	connection.connect(function(err) {
		console.log('connect to db');

		if (err) {
			console.log('error when connecting to db:', err);
			setTimeout(handleDisconnect, 2000);
		}
	});

	connection.on('error', function(err) {
		console.log('db error', err);
		if (err.code === 'PROTOCOL_CONNECTION_LOST') {
			handleDisconnect();
		} else {
			throw err;
		}
	});
}


io.sockets.on('connection', function(socket) {
	handleDisconnect();

	socket.emit('message', {
		message: 'welcome to the chat'
	});

	socket.on('send', function(data) {
		io.sockets.emit('message', data);

		connection.query('INSERT INTO messages VALUES(NULL,"' + data.username + '","' + data.message + '",NULL)', function(err, rows, fields) {
			if (err) {
				console.log(err);
			}
		});
	});
});


console.log("Listening on port " + port);